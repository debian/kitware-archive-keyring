`kitware-archive-keyring`
=========================

This is Kitware's APT keyring package, which enables clients to verify packages
from Kitware's APT repository at https://apt.kitware.com/. New keys are
generated and deployed on an annual basis, with keys being generated and added
to this package at the beginning of July, and deployed to sign
https://apt.kitware.com/ at the beginning of January. The previous year's key is
retired at the beginning of July.

This package installs two keyrings:

1. `/usr/share/keyrings/kitware-archive-keyring.gpg`
2. `/usr/share/keyrings/kitware-archive-removed-keys.gpg`

At any given moment, `kitware-archive-keyring.gpg` contains two keys: the
current year's key, and either last year's key or next year's key.
`kitware-archive-removed-keys.gpg` contains all keys that have been retired and
are no longer used to sign the repository.
