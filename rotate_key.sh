#!/bin/bash

set -e -x

# Make sure gpg is GnuPG 2.x
gpg --version | grep -q -E '^gpg \(GnuPG\) 2\.'

filename="$1"
next_year="$(($(date +%Y) + 1))"
prev_year="$(($(date +%Y) - 1))"
version="$(date +%Y.%m.%d)"

workdir=$(mktemp -d)
trap "rm -rf '$workdir'" EXIT
export GNUPGHOME="$workdir"

# Add new key
gpg --import "$filename"
diff_file="$(jetring-gen /dev/null "$workdir/pubring.kbx" "Add Kitware $next_year")"
mv "$diff_file" "$workdir/add-kitware-archive-$next_year"
jetring-accept kitware-archive-keyring/ "$workdir/add-kitware-archive-$next_year"

# Retire last year's key
prev_year_line="$(head -n1 kitware-archive-keyring/index)"
prev_year_filename="$(echo "$prev_year_line" | cut -d' ' -f3)"
new_index="$(tail -n+2 kitware-archive-keyring/index)"
echo "$new_index" > kitware-archive-keyring/index
mv "kitware-archive-keyring/$prev_year_filename" "kitware-archive-removed-keys/$prev_year_filename"
echo "$prev_year_line" >> kitware-archive-removed-keys/index

# Commit the changes
git add "kitware-archive-keyring/index" "kitware-archive-keyring/add-kitware-archive-$next_year" "kitware-archive-keyring/$prev_year_filename" "kitware-archive-removed-keys/index" "kitware-archive-removed-keys/$prev_year_filename"
git commit -m "Retire $prev_year key, add $next_year key"

# Release a new version
dch -v "$version" --distribution unstable "Add $next_year key, remove $prev_year key."
git add debian/changelog
git commit -m "Version $version"
git tag "v$version"
