cmake_minimum_required(VERSION 3.5)
project(kitware-archive-keyring NONE)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

find_package(Jetring REQUIRED)

jetring_build(
  kitware-archive-keyring "${CMAKE_SOURCE_DIR}/kitware-archive-keyring" ALL
  )

jetring_build(
  kitware-archive-removed-keys "${CMAKE_SOURCE_DIR}/kitware-archive-removed-keys" ALL
  )

install(
  FILES
    "${CMAKE_BINARY_DIR}/kitware-archive-keyring.gpg"
    "${CMAKE_BINARY_DIR}/kitware-archive-removed-keys.gpg"
  DESTINATION "/usr/share/keyrings"
  )
