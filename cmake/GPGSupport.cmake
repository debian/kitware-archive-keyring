include(FindPackageHandleStandardArgs)

string(TOUPPER ${GPG_NAME} GPG_UPPER)

if(DEFINED GPG_MAJOR_VERSION)
  set(_versioned_gpg gpg${GPG_MAJOR_VERSION})
  set(_gpg_doc "GPG ${GPG_MAJOR_VERSION} executable")
else()
  unset(_versioned_gpg)
  set(_gpg_doc "GPG executable")
endif()

find_program(
  ${GPG_UPPER}_EXECUTABLE
  NAMES ${_versioned_gpg} gpg
  DOC "${_gpg_doc}"
  )

if(${GPG_UPPER}_EXECUTABLE)
  execute_process(
    COMMAND "${${GPG_UPPER}_EXECUTABLE}" --version
    OUTPUT_VARIABLE _gpg_version_output
    )

  if("${_gpg_version_output}" MATCHES "^gpg \\(GnuPG\\) ([12])\\.([^\n]*)\n")
    set(${GPG_UPPER}_VERSION "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}")
    set(_found_major "${CMAKE_MATCH_1}")

    if(NOT DEFINED GPG_MAJOR_VERSION
       OR "${_found_major}" STREQUAL "${GPG_MAJOR_VERSION}")
      set(${GPG_UPPER}_VERSION_VALID TRUE)
    else()
      set(${GPG_UPPER}_VERSION_VALID FALSE)
    endif()
  endif()

  if(NOT TARGET ${GPG_NAME}::gpg)
    add_executable(${GPG_NAME}::gpg IMPORTED)
    set_property(
      TARGET ${GPG_NAME}::gpg
      PROPERTY IMPORTED_LOCATION "${${GPG_UPPER}_EXECUTABLE}"
      )
  endif()

  function(${GPG_NAME}_create_keyring KEYRING_NAME INPUT_FILE)
    set(_options ALL)
    set(_one_value_args)
    set(_multi_value_args)

    cmake_parse_arguments(
      _GPG
      "${_options}"
      "${_one_value_args}"
      "${_multi_value_args}"
      ${ARGN}
      )

    if(_GPG_ALL)
      set(_all ALL)
    else()
      unset(_all)
    endif()

    set(_keyring_file "${KEYRING_NAME}.gpg")
    if(NOT IS_ABSOLUTE _keyring_file)
      set(_keyring_file "${CMAKE_CURRENT_BINARY_DIR}/${_keyring_file}")
    endif()

    add_custom_command(
      OUTPUT "${_keyring_file}"
      COMMAND
        ${GPG_NAME}::gpg
          --no-options
          --no-default-keyring
          --no-auto-check-trustdb
          --trustdb-name "${CMAKE_CURRENT_BINARY_DIR}/trustdb.gpg"
          --no-keyring
          --import-options import-export
          --output "${_keyring_file}"
          --import ${INPUT_FILE}
      COMMENT "Creating keyring file ${_keyring_file}"
      DEPENDS ${INPUT_FILE}
      )

    add_custom_target(
      "${KEYRING_NAME}" ${_all}
      DEPENDS "${_keyring_file}"
      )
  endfunction()
endif()

find_package_handle_standard_args(
  ${GPG_NAME}
  REQUIRED_VARS ${GPG_UPPER}_EXECUTABLE ${GPG_UPPER}_VERSION_VALID
  VERSION_VAR ${GPG_UPPER}_VERSION
  )
