include(FindPackageHandleStandardArgs)

if(NOT JETRING_FIND_COMPONENTS)
  set(JETRING_FIND_COMPONENTS Build Accept)
  set(JETRING_FIND_REQUIRED_Build TRUE)
  set(JETRING_FIND_REQUIRED_Accept TRUE)
endif()

foreach(_component IN LISTS JETRING_FIND_COMPONENTS)
  set(JETRING_${_component}_FOUND FALSE)
endforeach()

set(_jetring_required_vars)

if("Build" IN_LIST JETRING_FIND_COMPONENTS)
  if(JETRING_FIND_REQUIRED_Build)
    list(APPEND _jetring_required_vars JETRING_BUILD_EXECUTABLE)
  endif()

  find_program(
    JETRING_BUILD_EXECUTABLE
    jetring-build
    DOC "jetring-build command"
    )

  if(JETRING_BUILD_EXECUTABLE)
    set(JETRING_Build_FOUND TRUE)

    if(NOT TARGET Jetring::Build)
      add_executable(Jetring::Build IMPORTED)
      set_property(
        TARGET Jetring::Build
        PROPERTY IMPORTED_LOCATION "${JETRING_BUILD_EXECUTABLE}"
        )
    endif()

    function(jetring_build KEYRING_NAME INPUT_DIR)
      set(_options ALL)
      set(_one_value_args)
      set(_multi_value_args)

      cmake_parse_arguments(
        _JETRING
        "${_options}"
        "${_one_value_args}"
        "${_multi_value_args}"
        ${ARGN}
        )

      if(_JETRING_ALL)
        set(_all ALL)
      else()
        unset(_all)
      endif()

      set(_keyring_file "${KEYRING_NAME}.gpg")
      if(NOT IS_ABSOLUTE _keyring_file)
        set(_keyring_file "${CMAKE_CURRENT_BINARY_DIR}/${_keyring_file}")
      endif()

      add_custom_command(
        OUTPUT "${_keyring_file}"
        COMMAND
          Jetring::Build
            "${_keyring_file}"
            "${INPUT_DIR}"
        COMMENT "Creating keyring file ${_keyring_file}"
        DEPENDS "${INPUT_DIR}/index"
        )

      add_custom_target(
        "${KEYRING_NAME}" ${_all}
        DEPENDS "${_keyring_file}"
        )
    endfunction()
  endif()
endif()

if("Accept" IN_LIST JETRING_FIND_COMPONENTS)
  if(JETRING_FIND_REQUIRED_Accept)
    list(APPEND _jetring_required_vars JETRING_ACCEPT_EXECUTABLE)
  endif()

  find_program(
    JETRING_ACCEPT_EXECUTABLE
    jetring-accept
    DOC "jetring-accept command"
    )

  if(JETRING_ACCEPT_EXECUTABLE)
    set(JETRING_Accept_FOUND TRUE)

    if(NOT TARGET Jetring::Accept)
      add_executable(Jetring::Accept IMPORTED)
      set_property(
        TARGET Jetring::Accept
        PROPERTY IMPORTED_LOCATION "${JETRING_ACCEPT_EXECUTABLE}"
        )
    endif()

    function(jetring_accept DIRNAME CHANGESET)
      set(_options ALL)
      set(_one_value_args)
      set(_multi_value_args)

      cmake_parse_arguments(
        _JETRING
        "${_options}"
        "${_one_value_args}"
        "${_multi_value_args}"
        ${ARGN}
        )

      if(_JETRING_ALL)
        set(_all ALL)
      else()
        unset(_all)
      endif()

      set(_output_dir "${DIRNAME}")
      if(NOT IS_ABSOLUTE _output_dir)
        set(_output_dir "${CMAKE_CURRENT_BINARY_DIR}/${_output_dir}")
      endif()
      set(_index_file "${_output_dir}/index")

      add_custom_command(
        OUTPUT "${_index_file}"
        COMMAND
          Jetring::Accept
            "${_output_dir}"
            "${CHANGESET}"
        COMMENT "Creating Jetring changeset ${_output_dir}"
        DEPENDS "${CHANGESET}"
        )

      add_custom_target(
        "${DIRNAME}" ${_all}
        DEPENDS "${_index_file}"
        )
    endfunction()
  endif()
endif()

find_package_handle_standard_args(
  Jetring
  REQUIRED_VARS ${_jetring_required_vars}
  HANDLE_COMPONENTS
  )
